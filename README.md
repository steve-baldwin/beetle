# Beetle

Beetle is a Web API load testing library written in [Crystal](https://crystal-lang.org/) and inspired by [Locust](https://locust.io/).

## Installation

  * Add the dependency to your application's `shard.yml`:
```yaml
dependencies:
    beetle:
        gitlab: steve-baldwin/beetle
```
  * Run `shards install`

## Usage

```crystal
require "beetle"

module MyBeetleTasks
  class SomeClass
    include JSON::Serializable
    property customer_id : String
  end

  class MyTask1 < Beetle::Task
    def submit
      payload = SomeClass.new
      @client.post("/endpoint1", body: payload.to_json.to_s)
    end
  end
end

s = Beetle::Server.new
s.register_task MyBeetleTasks::MyTask1
s.listen
```

Start by looking at the [documentation](https://steve-baldwin.gitlab.io/beetle/index.html) for `Server`, then `Task`, `Job` and `Exec`. Hopefully by then you should get an idea if or how this library might help you.

## Contributing

1. Fork it (<https://gitlab.com/steve-baldwin/beetle/forks/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [Steve Baldwin](https://gitlab.com/steve-baldwin) - creator and maintainer
