require "csv"
require "uuid"

module Beetle
  #
  # A subclass of `Archiver` that writes (appends) Job and Execution data to a series of `.csv` files.
  #
  # For example:
  # ```
  # archiver = Beetle::CSVArchiver.new(".")
  # s = Beetle::Server.new
  # s.archiver = archiver
  # :
  # s.listen
  # ```
  #
  class CSVArchiver < Archiver
    # :nodoc:
    TS_FORMAT = "%F %T.%L%:z"
    @dir : String

    #
    # Creates a new `CSVArchiver` setting the base directory to _path_. All csv files will
    # be created or appended to under this directory.
    #
    def initialize(path : String)
      raise "#{path} is not a directory" unless File.directory?(path)
      @dir = path
    end

    #
    # Creates (or appends to) a series of csv files under _path_ on completion of a Job execution.
    # The files and their corresponding fields are:
    #
    # `beetle-job.csv` - Job Details
    #  * Unique Job ID (UUID)
    #  * Job Name
    #  * Host (may be null)
    #  * Maximum run time in seconds
    #  * Number of requests to be made by each swarm member
    #  * Number in the swarm
    #  * Execution timestamp (in UTC)
    #  * Elapsed time (in microseconds) to execute the Job
    #  * Threshold was exceeded (1) or not (0)
    #
    # `beetle-job-params.csv` - Job Parameters
    #  * Job ID (contained in `beetle-job.csv`)
    #  * Parameter name
    #  * Parameter value
    #
    # `beetle-job-task.csv` - Tasks defined for the Job
    #  * Job ID (contained in `beetle-job.csv`)
    #  * Task Class (subclass of `Beetle::Task`)
    #  * Host (refer to `JobTask#host`)
    #  * Task Weight (refer to `JobTask#weight`)
    #  * Min sleep time between requests in milliseconds (refer to `JobTask#sleep_min_msec`)
    #  * Max sleep time between requests in milliseconds (refer to `JobTask#sleep_max_msec`)
    #  * Threshold average execution time in microseconds (refer to `JobTask#threshold_avg_usec`)
    #  * Threshold maximum execution time in microseconds (refer to `JobTask#threshold_avg_usec`)
    #  * Number of executions of this Task Class
    #  * Number of successful executions
    #  * Number of failed executions
    #  * Minimum elapsed time of all executions in microseconds
    #  * Maximum elapsed time of all executions in microseconds
    #  * Average elapsed time of all executions in microseconds
    #  * Total elapsed time of all executions in microseconds
    #  * Threshold average execution time was exceeded (1) or not (0)
    #  * Threshold maximum execution time was exceeded (1) or not (0)
    #
    # `beetle-task-exec.csv` - Individual task requests made by swarm worker(s)
    #  * Job ID (contained in `beetle-job.csv`)
    #  * Unique request ID (UUID. Set in `X-Request-ID` header of http request)
    #  * Id of worker in swarm
    #  * Task Class (subclass of `Beetle::Task`)
    #  * Start Timestamp (in UTC)
    #  * Elapsed time in microseconds
    #  * Error body
    #  * Success (1) or failure (0)
    #
    def archive(job_exec : JobExec)
      exec_id = UUID.random.to_s
      f = File.new(File.join(@dir, "beetle-job.csv"), "a")
      CSV.build(f) do |csv|
        csv.row(
          job_exec.id,
          job_exec.name,
          job_exec.host,
          job_exec.run_for_sec,
          job_exec.run_n,
          job_exec.n_swarm,
          job_exec.t_start.to_s(TS_FORMAT),
          job_exec.elapsed_usec,
          job_exec.threshold_exceeded
        )
      end
      f.close

      if job_exec.params.keys.size > 0
        f = File.new(File.join(@dir, "beetle-job-params.csv"), "a")
        CSV.build(f) do |csv|
          job_exec.params.each do |key, val|
            csv.row(job_exec.id, key, val)
          end
        end
        f.close
      end

      f = File.new(File.join(@dir, "beetle-job-task.csv"), "a")
      CSV.build(f) do |csv|
        job_exec.tasks.each do |task_class, exec|
          csv.row(
            job_exec.id,
            task_class,
            exec.host,
            exec.weight,
            exec.sleep_min_msec,
            exec.sleep_max_msec,
            exec.threshold_avg_usec,
            exec.threshold_max_usec,
            exec.n_task,
            exec.n_success,
            exec.n_fail,
            exec.min_usec,
            exec.max_usec,
            exec.avg_usec,
            exec.tot_usec,
            exec.threshold_avg_exceeded,
            exec.threshold_max_exceeded
          )
        end
      end
      f.close

      f = File.new(File.join(@dir, "beetle-task-exec.csv"), "a")
      CSV.build(f) do |csv|
        job_exec.tasks.each do |task_class, exec|
          exec.executions.each do |x|
            csv.row(
              job_exec.id,
              x.id,
              x.swarm_id,
              task_class,
              x.t_start.to_s(TS_FORMAT),
              x.elapsed_usec,
              x.request_context,
              x.error_info,
              x.is_success
            )
          end
        end
      end
      f.close
    rescue ex
      puts "Exception: #{ex.message}"
      puts ex.inspect_with_backtrace
    end
  end
end
