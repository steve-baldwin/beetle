require "db"
require "uuid"

module Beetle
  #
  # A subclass of `Archiver` that saves Job and Execution data to a series of database tables.
  #
  # The database must have been previously prepared from one of the initialisation scripts
  # in the `sql` directory. There are scripts for Postgresql (`pg-archiver-tables.sql`) and
  # Sqlite (`sqlite3-archiver-tables.sql`).
  #
  # This class uses the standard [Crystal Database API](http://crystal-lang.github.io/crystal-db/api/0.5.0/)
  # so any database engine supporting this API should work as long as you can adapt one of the
  # provided initialisation scripts.
  #
  # Sample usage:
  # ```
  # db = DB.open "sqlite3://./beetle.db"
  # archiver = Beetle::DBArchiver.new(db)
  # s = Beetle::Server.new
  # s.archiver = archiver
  # :
  # s.listen
  # ```
  #
  class DBArchiver < Archiver
    # :nodoc:
    INS_1 = <<-END_SQL
    INSERT
    INTO    beetle_job(
              id,
              name,
              host,
              run_for_sec,
              run_n,
              n_swarm,
              start_ts,
              elapsed_usec,
              threshold_exceeded
              )
    VALUES  ($1, $2, $3, $4, $5, $6, $7, $8, $9)
    END_SQL

    # :nodoc:
    INS_2 = <<-END_SQL
    INSERT
    INTO    beetle_job_param(job_id, key, value)
    VALUES  ($1, $2, $3)
    END_SQL

    # :nodoc:
    INS_3 = <<-END_SQL
    INSERT
    INTO    beetle_job_task(
              job_id,
              task_class,
              host,
              weight,
              sleep_min_msec,
              sleep_max_msec,
              threshold_avg_usec,
              threshold_max_usec,
              n_task,
              n_success,
              n_fail,
              min_elapsed_usec,
              max_elapsed_usec,
              avg_elapsed_usec,
              tot_elapsed_usec,
              threshold_avg_exceeded,
              threshold_max_exceeded
              )
    VALUES  ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17)
    END_SQL

    # :nodoc:
    INS_4 = <<-END_SQL
    INSERT
    INTO    beetle_task_exec(
              id,
              job_id,
              swarm_id,
              task_class,
              start_ts,
              elapsed_usec,
              request_context,
              error_info,
              success
              )
    VALUES  ($1, $2, $3, $4, $5, $6, $7, $8, $9)
    END_SQL

    #
    # Creates a new `DBArchiver` setting the database handle to _db_. This handle will be used to do all database inserts.
    #
    def initialize(@db : DB::Database, @init_path : String)
      tables = %w(beetle_job beetle_job_param beetle_job_task beetle_task_exec)
      begin
        tables.each { |t| n = @db.scalar("select count(*) from #{t}") }
      rescue exception
        script = File.read(@init_path)
        script.split(';').each do |stmt|
          x = stmt.lstrip.rstrip
          @db.exec(x) if x.size > 0
        end
      end
    end

    #
    # Insert into a series of database tables on completion of a Job execution.
    #
    # The tables are (`postgres` definition shown)
    #
    # `beetle_job`:
    # ```sql
    #                            Table "public.beetle_job"
    #        Column       |           Type           | Collation | Nullable | Default
    # --------------------+--------------------------+-----------+----------+---------
    #  id                 | uuid                     |           | not null |
    #  name               | text                     |           | not null |
    #  host               | text                     |           |          |
    #  run_for_sec        | integer                  |           |          |
    #  run_n              | integer                  |           |          |
    #  n_swarm            | integer                  |           |          |
    #  start_ts           | timestamp with time zone |           |          |
    #  elapsed_usec       | bigint                   |           |          |
    #  threshold_exceeded | boolean                  |           |          |
    # Indexes:
    #  "beetle_job_pkey" PRIMARY KEY, btree (id)
    # ```
    # `beetle_job_param`:
    # ```sql
    #         Table "public.beetle_job_param"
    #  Column | Type | Collation | Nullable | Default
    # --------+------+-----------+----------+---------
    #  job_id | uuid |           | not null |
    #  key    | text |           | not null |
    #  value  | text |           |          |
    # Indexes:
    #  "beetle_job_param_pkey" PRIMARY KEY, btree (job_id, key)
    # ```
    # `beetle_job_task`:
    # ```sql
    #                   Table "public.beetle_job_task"
    #          Column         |  Type   | Collation | Nullable | Default
    # ------------------------+---------+-----------+----------+---------
    #  job_id                 | uuid    |           | not null |
    #  task_class             | text    |           | not null |
    #  host                   | text    |           | not null |
    #  weight                 | integer |           |          |
    #  sleep_min_msec         | integer |           |          |
    #  sleep_max_msec         | integer |           |          |
    #  threshold_avg_usec     | integer |           |          |
    #  threshold_max_usec     | integer |           |          |
    #  n_task                 | integer |           | not null |
    #  n_success              | integer |           | not null |
    #  n_fail                 | integer |           | not null |
    #  min_elapsed_usec       | integer |           |          |
    #  max_elapsed_usec       | integer |           |          |
    #  avg_elapsed_usec       | integer |           |          |
    #  tot_elapsed_usec       | integer |           |          |
    #  threshold_avg_exceeded | boolean |           |          |
    #  threshold_max_exceeded | boolean |           |          |
    # Indexes:
    #  "beetle_job_task_pkey" PRIMARY KEY, btree (job_id, task_class)
    # ```
    # `beetle_task_exec`:
    # ```sql
    #                         Table "public.beetle_task_exec"
    #      Column      |           Type           | Collation | Nullable | Default
    # -----------------+--------------------------+-----------+----------+---------
    #  id              | uuid                     |           | not null |
    #  job_id          | uuid                     |           | not null |
    #  swarm_id        | integer                  |           | not null |
    #  task_class      | text                     |           | not null |
    #  start_ts        | timestamp with time zone |           | not null |
    #  elapsed_usec    | integer                  |           | not null |
    #  request_context | text                     |           |          |
    #  error_info      | text                     |           |          |
    #  success         | boolean                  |           | not null |
    # Indexes:
    #     "beetle_task_exec_pkey" PRIMARY KEY, btree (id)
    # ```
    #
    def archive(job_exec : JobExec)
      @db.transaction do |trx|
        db_t = trx.connection
        db_t.exec(
          INS_1,
          job_exec.id,
          job_exec.name,
          job_exec.host,
          job_exec.run_for_sec,
          job_exec.run_n,
          job_exec.n_swarm,
          job_exec.t_start,
          job_exec.elapsed_usec,
          job_exec.threshold_exceeded
        )
        if job_exec.params.keys.size > 0
          job_exec.params.each do |key, val|
            db_t.exec(
              INS_2,
              job_exec.id,
              key,
              val
            )
          end
        end
        job_exec.tasks.each do |task_class, exec|
          db_t.exec(
            INS_3,
            job_exec.id,
            task_class,
            exec.host,
            exec.weight,
            exec.sleep_min_msec,
            exec.sleep_max_msec,
            exec.threshold_avg_usec,
            exec.threshold_max_usec,
            exec.n_task,
            exec.n_success,
            exec.n_fail,
            exec.min_usec,
            exec.max_usec,
            exec.avg_usec,
            exec.tot_usec,
            exec.threshold_avg_exceeded,
            exec.threshold_max_exceeded
          )
        end
        job_exec.tasks.each do |task_class, exec|
          exec.executions.each do |x|
            db_t.exec(
              INS_4,
              x.id,
              job_exec.id,
              x.swarm_id,
              task_class,
              x.t_start,
              x.elapsed_usec,
              x.request_context,
              x.error_info,
              x.is_success
            )
          end
        end
      end
    rescue ex
      puts "Exception: #{ex.message}"
      puts ex.inspect_with_backtrace
    end
  end
end
