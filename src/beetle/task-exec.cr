require "json"

module Beetle
  #
  # Captures information about a single execution of a `Task` in the context of a `Job`.
  #
  # You never instantiate this class directly. It is managed internally by Beetle, but is
  # made available to an archiver as part of the `job_exec` argument in the
  # `Archiver#archive` method.
  #
  # It contains the following fields:
  #  * `id` - A unique (UUID) identifier for this specific task execution.
  #  * `swarm_id` - The id of the submitting swarm member (1 .. `Job#n_swarm`)
  #  * `t_start` - The clock time (in UTC) when the http request was submitted.
  #  * `elapsed_usec` - The elapsed time in microseconds before a response was received.
  #  * `request_context` - Anything the Task chooses to record as contextual information
  # on the request.
  #  * `is_success` - Boolean set to `true` if the response code was in the `2xx` range.
  #
  class TaskExec
    include JSON::Serializable
    # :nodoc:
    property id : String
    # :nodoc:
    property swarm_id : Int32
    @[JSON::Field(converter: Time::Format.new("%F %T.%L%:z"))]
    # :nodoc:
    property t_start : Time
    # :nodoc:
    property elapsed_usec : Int32
    # :nodoc:
    property request_context : String?
    # :nodoc:
    property is_success : Bool
    # :nodoc:
    property error_info : String?

    # :nodoc:
    def_clone

    # :nodoc:
    def initialize(@id, @swarm_id, @t_start, @elapsed_usec, @request_context, @is_success, @error_info)
    end
  end
end
