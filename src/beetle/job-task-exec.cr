require "json"
require "./task-exec"

module Beetle
  #
  # Defines the statistics captured for an individual `Task` subclass as part of
  # a `Job` execution.
  #
  # As with `JobExec`, you never instantiate this class directly.
  #
  # It contains the following fields:
  #  * `host` - The actual host URL used for a `Task` execution
  #  * `weight` - See `JobTask#weight`
  #  * `sleep_min_msec` - See `JobTask#sleep_min_msec`
  #  * `sleep_max_msec` - See `JobTask#sleep_max_msec`
  #  * `threshold_avg_usec` - See `JobTask#threshold_avg_usec`
  #  * `threshold_max_usec` - See `JobTask#threshold_max_usec`
  #  * `n_task` - The number of times this task was executed for this `Job` execution.
  #  * `n_success` - The number of times the task execution succeeded (return `2xx`).
  #  * `n_fail` - The number of times the task execution failed (return not `2xx`).
  #  * `min_usec` - The minimum task execution duration in microseconds.
  #  * `max_usec` - The maxumum task execution duration in microseconds.
  #  * `avg_usec` - The average task execution duration in microseconds.
  #  * `tot_usec` - The total task execution duration in microseconds.
  #  * `threshold_avg_exceeded` - Set to `true` if `threshold_avg_usec` is non-zero for
  # a `JobTask` and the `avg_usec` value exceeds it.
  #  * `threshold_max_exceeded` - Set to `true` if `threshold_max_usec` is non-zero for
  # a `JobTask` and the `max_usec` value exceeds it.
  #
  class JobTaskExec
    include JSON::Serializable
    # :nodoc:
    property host : String
    # :nodoc:
    property weight : Int32
    # :nodoc:
    property sleep_min_msec : Int32 = 0
    # :nodoc:
    property sleep_max_msec : Int32 = 0
    # :nodoc:
    property threshold_avg_usec : Int32 = 0
    # :nodoc:
    property threshold_max_usec : Int32 = 0
    # :nodoc:
    property n_task : Int32 = 0
    # :nodoc:
    property n_success : Int32 = 0
    # :nodoc:
    property n_fail : Int32 = 0
    # :nodoc:
    property min_usec : Int32 = Int32::MAX
    # :nodoc:
    property max_usec : Int32 = Int32::MIN
    # :nodoc:
    property avg_usec : Int32 = 0
    # :nodoc:
    property tot_usec : Int64 = 0
    # :nodoc:
    property threshold_avg_exceeded = false
    # :nodoc:
    property threshold_max_exceeded = false
    # :nodoc:
    property executions : Array(Beetle::TaskExec) = [] of Beetle::TaskExec

    # :nodoc:
    def_clone

    # :nodoc:
    def initialize(job_task : JobTask)
      @host = job_task.host.not_nil!
      @weight = job_task.weight
      @sleep_min_msec = job_task.sleep_min_msec
      @sleep_max_msec = job_task.sleep_max_msec
      @threshold_avg_usec = job_task.threshold_avg_usec
      @threshold_max_usec = job_task.threshold_max_usec
    end

    # :nodoc:
    def derive_stats : Bool
      @n_task = @executions.size
      threshold_exceeded = false
      @executions.each do |x|
        if x.is_success
          @n_success += 1
          @tot_usec += x.elapsed_usec
          @min_usec = x.elapsed_usec if x.elapsed_usec < @min_usec
          @max_usec = x.elapsed_usec if x.elapsed_usec > @max_usec
        else
          @n_fail += 1
        end
      end
      if @n_success > 0
        @avg_usec = (@tot_usec.to_f64 / @n_success).to_i32
      else
        @min_usec = 0
        @max_usec = 0
      end
      if @threshold_avg_usec > 0
        @threshold_avg_exceeded = @avg_usec > @threshold_avg_usec
      end
      if @threshold_max_usec > 0
        @threshold_max_exceeded = @max_usec > @threshold_max_usec
      end
      @threshold_avg_exceeded || @threshold_max_exceeded
    end

    # :nodoc:
    def count_success : Int32
      executions.reduce(0) { |memo, e| memo + (e.is_success ? 1 : 0) }
    end

    # :nodoc:
    def count_failure : Int32
      executions.reduce(0) { |memo, e| memo + (e.is_success ? 0 : 1) }
    end

    # :nodoc:
    def clear_executions : Nil
      @executions.clear
    end
  end
end
