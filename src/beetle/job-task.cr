require "json"

module Beetle
  #
  # Defines a task to be executed as part of a `Job`. As with `Job`, this class is
  # never created directly but rather by the `Server` object from the JSON payload associated
  # with the `/job` request. The fields in the JSON payload are:
  #  * `task_class` - The fully qualified name of the crystal class representing this task.
  #  * `weight` - The average number of times this task should be chosen from all the tasks
  # for a job. Tasks are randomly chosen but a task with a `weight` of 2 is twice as likely
  # to be chosen as a task with a `weight` of 1.
  #  * `sleep_min_msec` - The minumum time (in milliseconds) to sleep after executing this task.
  #  * `sleep_max_msec` - The maximum time (in milliseconds) to sleep after executing this task.
  #  * `threshold_avg_usec` - The acceptable request threshold (in microseconds) for the average
  # respoonse time. A value of zero (the default) means 'no threshold'.
  #  * `threshold_max_usec` - The acceptable request threshold (in microseconds) for the maximum
  # respoonse time. A value of zero (the default) means 'no threshold'.
  #
  # The actual sleep time is derived as a random (integer) value between `sleep_min_msec` and `sleep_max_msec`.
  #
  # If a job task has a non-zero threshold defined and that threshold is exceeded during an execution of
  # that job, and the `/exec` request payload set `error_400_on_threshold_exceeded` to `true`,
  # the server will respond to the `/exec` request with a `400` return code.
  #
  # For example (from a `/job` payload):
  # ```json
  # {
  #     :
  #     "tasks": [
  #         {
  #             "task_class": "BCaaS::ChargeTask",
  #             "weight": 3,
  #             "sleep_min_msec": 500,
  #             "sleep_max_msec": 1000
  #         },
  #         {
  #             "task_class": "BCaaS::Preauthorization",
  #             "weight": 1,
  #             "sleep_min_msec": 250,
  #             "sleep_max_msec": 750
  #         }
  #     ]
  # }
  # ```
  #
  class JobTask
    include JSON::Serializable
    # :nodoc:
    property task_class : String
    # :nodoc:
    property weight : Int32 = 1
    # :nodoc:
    property sleep_min_msec : Int32 = 0
    # :nodoc:
    property sleep_max_msec : Int32 = 0
    # :nodoc:
    property threshold_avg_usec : Int32 = 0
    # :nodoc:
    property threshold_max_usec : Int32 = 0
    # :nodoc:
    property host : String?
  end
end
