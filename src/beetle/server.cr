require "http/server"
require "uri"
require "uuid"
require "log"
require "./archiver"
require "./exec"
require "./job"
require "./task"
require "./task-list"

module Beetle
  #
  # The Server class is the co-ordinator of the `Beetle` subsystem.
  # The basic process is:
  #  * Create an instance of the `Server` class
  #  * Optionally create an archiver and register it to your server
  #  * Register all your individual `Task` subclasses that actually do the work you want to measure
  #  * Run the `#listen` method to 'wait for instructions'.
  #
  # The server listens on the following endpoints:
  #  * `/job` - Accept a json payload representing a new `Job`. This doesn't actually run the job.
  #    Rather it saves it to be run by the `/exec` request.
  #  * `/exec` - Execute a previously saved job. Only one job execution can be in progress at any
  #    point in time. If you submit a `/exec` request while another is still in progress, it will
  #    be rejected.
  #  * `/tasks` - Return a list of registered `Task` subclasses that may be used in a `Job` definition.
  #  * `/jobs` - Return a list of saved jobs.
  #  * `/status` - Return a JSON encoded `ServerStatus` object showing the current server state.
  #  * `/quit` - Stop listening
  #
  class Server
    @in_progress : String? = nil
    @job_exec : JobExec?
    @all_tasks : Hash(String, Task.class) = {} of String => Task.class
    @done = Channel(Nil).new
    @jobs : Hash(String, Job) = {} of String => Job
    @logger : Log
    @archiver : Archiver?
    @async_queue : Array(Exec) = [] of Exec
    @rnd : Random = Random.new

    #
    # Create a new `Server` instance.
    #
    def initialize
      backend = Log::IOBackend.new(STDOUT)
      backend.formatter = Log::Formatter.new do |entry, io|
        io << entry.severity.label[0]
        io << "|" << entry.source
        io << "|" << entry.timestamp.to_s("%H:%M:%S.%6N")
        io << "|" << Fiber.current.name
        io << "|" << entry.message
      end
      builder = Log::Builder.new
      level = Log::Severity.parse(ENV.fetch("CRYSTAL_LOG_LEVEL", "INFO"))
      builder.bind("*", level, backend)
      @logger = builder.for("beetle")
    end

    #
    # Set the optional archiver to be run on completion of a job execution.
    #
    def archiver=(archiver : Archiver)
      @archiver = archiver
    end

    #
    # Register a subclass of `Task` that is available to be part of a `Job`. If you submit
    # a `/job` request referencing an unregistered `Task` class you will receive a 400 error
    # with a message of `Unknown Task class(es): ...`.
    #
    def register_task(task_class : Task.class)
      @all_tasks[task_class.name] = task_class
    end

    private def exec_job(response : HTTP::Server::Response?, job, exec)
      run_n = job.run_n
      run_for_sec = job.run_for_sec
      n_tasks = job.tasks.size
      return unless (run_for_sec > 0) || (run_n > 0)
      return unless job.n_swarm > 0
      return unless n_tasks > 0
      @logger.warn { "Executing job [#{job.name}] with #{job.n_swarm} in the swarm" }
      run_until : Time? = nil
      if run_for_sec > 0
        run_until = Time.utc + run_for_sec.seconds
      end
      total_weight = 0
      less_than = [] of Int32
      job.tasks.each do |t|
        total_weight += t.weight
        less_than << total_weight
      end
      done = Channel(Nil).new
      start : Array(Channel(Nil)) = [] of Channel(Nil)
      job_exec = JobExec.new(job, exec)
      @job_exec = job_exec
      job.n_swarm.times do |n_kid|
        c = Channel(Nil).new
        fname = Fiber.current.name
        spawn do
          #
          # We want any log entries for this request to be linked together,
          # regardless of the worker.
          #
          Fiber.current.name = fname
          me = n_kid + 1
          my_start = c
          my_run_n = run_n
          #
          # Instantiate the Task objects and any HTTP::Client objects I'm
          # going to need.
          #
          my_tasks : Array(Task) = [] of Task
          my_task_classes : Array(String) = [] of String
          host_client : Hash(String, HTTP::Client) = {} of String => HTTP::Client
          my_clients : Array(HTTP::Client) = [] of HTTP::Client
          job.tasks.each do |job_task|
            task_class = job_task.task_class
            my_task_classes << task_class
            t = @all_tasks[task_class].new(@logger, me, exec.params)
            my_tasks << t
            host = job_task.host.not_nil!
            unless host_client.has_key?(host)
              uri = URI.parse(host)
              client = HTTP::Client.new(uri)
              client.connect_timeout = 1
              client.read_timeout = 5
              client.write_timeout = 1
              client.before_request do |r|
                if request_id = client.request_id
                  r.headers["X-Request-ID"] = request_id
                end
                if p = uri.path
                  r.path = "#{p}#{r.path}"
                end
              end
              host_client[host] = client
            end
            my_clients << host_client[host]
          end
          #
          # Wait for the go-ahead
          #
          my_start.receive
          #
          # Main loop
          #
          child_done = false
          while !child_done
            #
            # Which task are we going to execute?
            #
            n_task = 0
            if n_tasks > 1
              r = Random.rand(total_weight)
              (n_tasks - 1).downto(0) do |i|
                if r >= less_than[i]
                  n_task = i + 1
                  break
                end
              end
            end
            job_task = job.tasks[n_task]
            task = my_tasks[n_task]
            client = my_clients[n_task]
            my_request_id = UUID.random.to_s
            client.request_id = my_request_id
            task.request_context = nil
            t1 = Time.utc
            task_ok = false
            error_info : String?
            begin
              task_response = task.submit(client)
              task_ok = task_response.success?
              if task_ok
                error_info = nil
              else
                error_info = "status: #{task_response.status}[#{task_response.status_code}]#{task_response.body.size > 0 ? %(, body: ) + task_response.body : %()}"
              end
            rescue ex
              error_info = ex.to_s
            end
            e = ((Time.utc - t1).total_nanoseconds / 1_000).to_i32
            x = TaskExec.new(my_request_id, me, t1, e, task.request_context, task_ok, error_info)
            job_exec.tasks[my_task_classes[n_task]].executions << x
            unless task_ok
              @logger.warn { "#{task.class.to_s} failure: #{x.to_pretty_json}" } if job_exec.report_failures
              job_exec.n_failure += 1
            end
            #
            # Are we done yet?
            #
            if job_exec.n_failure >= job_exec.max_failure_count
              child_done = true
            elsif run_until
              child_done = Time.utc >= run_until.not_nil!
            else
              my_run_n -= 1
              child_done = my_run_n < 1
            end
            #
            # Do I need to sleep before submitting the next request?
            #
            unless child_done
              if job_task.sleep_max_msec > 0
                delta = job_task.sleep_max_msec - job_task.sleep_min_msec
                sleep_ms = job_task.sleep_min_msec
                if delta > 0
                  sleep_ms += Random.rand(delta)
                end
                sleep sleep_ms.milliseconds
              end
            end
          end
          #
          # Tell parent I'm done
          #
          @logger.warn { "[#{me}] is done" }
          done.send(nil)
        end
        start << c
      end
      #
      # Tell all the fibers to start
      #
      job.n_swarm.times do |i|
        start[i].send(nil)
      end
      #
      # Start the job watcher
      #
      if job_exec.report_progress
        spawn do
          Fiber.current.name = "JobWatcher"
          #
          # Wake every 0.5 seconds but report progress every 10 seconds
          #
          last_report = Time.utc
          while @job_exec
            if (Time.utc - last_report).seconds >= 10
              @logger.warn { @job_exec.not_nil!.progress_summary }
              last_report = Time.utc
            end
            sleep 500.milliseconds
          end
        end
      end
      #
      # Wait for each of the fibers to complete
      #
      job.n_swarm.times do
        done.receive
      end
      #
      # Derive task-based stats
      #
      job_exec.derive_stats
      #
      # Set the job execution duration
      #
      job_exec.elapsed_usec = ((Time.utc - job_exec.t_start).total_nanoseconds / 1_000).to_i64
      #
      # Archive the job if we have an archiver
      #
      if archiver = @archiver
        archiver.archive(job_exec)
      end

      if response
        response.status_code = 400 if job_exec.threshold_exceeded && job_exec.error_400_on_threshold_exceeded
        response.headers["Content-Type"] = "application/json"
        response.print job_exec.to_json.to_s
      else
        h = HTTP::Headers.new
        h["Content-Type"] = "application/json"
        begin
          HTTP::Client.post(exec.webhook_url.not_nil!, headers: h, body: job_exec.to_json.to_s)
        rescue ex
          puts ex.inspect_with_backtrace
        end
      end
      job_exec.clear_executions
      @logger.warn { "Completed execution of [#{job.name}]:\n#{job_exec.to_pretty_json}" }
      @job_exec = nil
    end

    #
    # Listen for instructions. The endpoints are described above. The payloads are:
    #
    #  * `/job` - A JSON representation of a `Job`. For example:
    # ```json
    # {
    #     "name": "charges_local",
    #     "host": "http://localhost:3001",
    #     "run_for_sec": 10,
    #     "tasks": [
    #         {
    #             "task_class": "BCaaS::ChargeTask",
    #             "weight": 1,
    #             "sleep_min_msec": 500,
    #             "sleep_max_msec": 1000
    #         }
    #     ]
    # }
    # ```
    # If the request is valid, the job will be stored and available to be
    # run with the `/exec` request. The response body will simply be `Job saved`.
    #
    #  * `/exec` - A JSON representation of an `Exec` object. For example:
    # ```json
    #   {
    #       "name": "charges_local",
    #       "params": {
    #           "sc_mc": "mc"
    #       }
    #   }
    # ```
    # A valid request will result in the named job being executed synchronously. On
    # completion the response body will contain a JSON representation of a `JobExec`
    # object. For example:
    # ```json
    # {
    #   "id": "77f2d06c-be37-4bdc-98ad-02fed5acd892",
    #   "name": "charges_local_2",
    #   "host": "http://localhost:3001",
    #   "t_start": "2019-02-19 21:36:47.430+00:00",
    #   "elapsed_usec": 750782,
    #   "params": {
    #       "sc_mc": "mc"
    #   },
    #   "error_400_on_threshold_exceeded": false,
    #   "tasks": {
    #       "BCaaS::ChargeTask": {
    #           "host": "http://localhost:3001",
    #           "weight": 1,
    #           "sleep_min_msec": 500,
    #           "sleep_max_msec": 1000,
    #           "threshold_avg_usec": 0,
    #           "threshold_max_usec": 0,
    #           "n_task": 2,
    #           "n_success": 2,
    #           "n_fail": 0,
    #           "min_usec": 448,
    #           "max_usec": 1980,
    #           "avg_usec": 1214,
    #           "tot_usec": 2428,
    #           "executions": [
    #               {
    #                   "id": "80b65420-2902-4e44-8676-6e2f620003af",
    #                   "swarm_id": 1,
    #                   "t_start": "2019-02-19 21:36:47.430+00:00",
    #                   "elapsed_usec": 1980,
    #                   "request_context": "buyer_id: 5066112c-b321-40a0-ac61-a7868b1921ee",
    #                   "is_success": true
    #               },
    #               {
    #                   "id": "5f173b79-9e11-423b-9fe9-e204c669af3a",
    #                   "swarm_id": 1,
    #                   "t_start": "2019-02-19 21:36:48.180+00:00",
    #                   "elapsed_usec": 448,
    #                   "request_context": "buyer_id: 45672a98-281a-4ee3-857a-89ea27fa2dcb",
    #                   "is_success": true
    #               }
    #           ],
    #           "threshold_avg_exceeded": false,
    #           "threshold_max_exceeded": false
    #       }
    #   },
    #   "run_for_sec": 0,
    #   "run_n": 2,
    #   "n_swarm": 1,
    #   "threshold_exceeded": false
    # }
    # ```
    #  * `/tasks`
    # This endpoint doesn't require a payload. It returns a JSON representation of the
    # list of registered (via `Server#register_task`) tasks. For example:
    # ```json
    # {
    #   "tasks": [
    #       "BCaaS::ChargeTask",
    #       "BCaaS::PreAuthTask"
    #   ]
    # }
    # ```
    #  * `/jobs`
    # This endpoint doesn't require a payload. It returns a JSON representation of all
    # the jobs available for execution (via the `/exec` endpoint). For example:
    # ```json
    # {
    #   "charges_local_2": {
    #       "name": "charges_local_2",
    #       "host": "http://localhost:3001",
    #       "tasks": [
    #           {
    #               "task_class": "BCaaS::ChargeTask",
    #               "weight": 1,
    #               "sleep_min_msec": 500,
    #               "sleep_max_msec": 1000,
    #               "threshold_avg_usec": 0,
    #               "threshold_max_usec": 0,
    #               "host": "http://localhost:3001"
    #           }
    #       ],
    #       "run_for_sec": 0,
    #       "run_n": 2,
    #       "n_swarm": 1
    #   },
    #   "charges_staging": {
    #       "name": "charges_staging",
    #       "tasks": [
    #           {
    #               "task_class": "BCaaS::ChargeTask",
    #               "weight": 1,
    #               "sleep_min_msec": 500,
    #               "sleep_max_msec": 1000,
    #               "threshold_avg_usec": 0,
    #               "threshold_max_usec": 0,
    #               "host": "https://app.bcaas-staging.msts.com/api/v20180807"
    #           }
    #       ],
    #       "run_for_sec": 10,
    #       "run_n": 0,
    #       "n_swarm": 1
    #   },
    #   "charges_5_staging": {
    #       "name": "charges_5_staging",
    #       "tasks": [
    #           {
    #               "task_class": "BCaaS::ChargeTask",
    #               "weight": 1,
    #               "sleep_min_msec": 0,
    #               "sleep_max_msec": 0,
    #               "threshold_avg_usec": 0,
    #               "threshold_max_usec": 0,
    #               "host": "https://app.bcaas-staging.msts.com/api/v20180807"
    #           }
    #       ],
    #       "run_for_sec": 10,
    #       "run_n": 0,
    #       "n_swarm": 5
    #   }
    # }
    # ```
    #  * `/quit` - Terminate the server. No payload is required.
    #
    def listen
      server = HTTP::Server.new do |context|
        Fiber.current.name = @rnd.hex(4)
        @logger.warn { "Received %s" % context.request.resource }
        case context.request.resource
        when "/quit"
          @done.send(nil)
        when "/tasks"
          context.response.content_type = "application/json"
          context.response.print TaskList.new(@all_tasks.keys).to_json.to_s
        when "/jobs"
          context.response.content_type = "application/json"
          context.response.print @jobs.to_json.to_s
        when "/status"
          state = ServerStatus.new(@job_exec, @async_queue, @all_tasks, @jobs)
          context.response.content_type = "application/json"
          context.response.print state.to_json.to_s
        when "/job"
          begin
            io = context.request.body.not_nil!
            body = io.gets_to_end
            job = Job.from_json(body)
            bad_tasks = [] of String
            job_host = job.host
            job.tasks.each do |task|
              if @all_tasks[task.task_class]?
                #
                # Do we have a host for every task for this job?
                #
                if job_host
                  task.host = job_host
                else
                  t = @all_tasks[task.task_class].new(@logger, 0, {} of String => String)
                  if task_host = t.default_host
                    task.host = task_host
                  else
                    bad_tasks << task.task_class
                  end
                end
              else
                bad_tasks << task.task_class
              end
            end
            if bad_tasks.size == 0
              @jobs[job.name] = job
              context.response.print "Job saved"
            else
              context.response.status_code = 400
              context.response.print "Unknown Task class(es): %s" % bad_tasks.join(",")
            end
          rescue exception
            puts exception.inspect_with_backtrace
            context.response.status_code = 400
            context.response.print "Invalid request"
          end
        when "/exec"
          begin
            io = context.request.body.not_nil!
            body = io.gets_to_end
            exec = Exec.from_json(body)
            if job = @jobs[exec.name]?
              if exec.webhook_url
                exec.error_400_on_threshold_exceeded = false
                @async_queue << exec
                context.response.print "Execution request enqueued"
              else
                if @in_progress
                  context.response.status_code = 400
                  context.response.print "Job execution is in progress"
                else
                  @in_progress = exec.name
                  exec_job(context.response, job, exec)
                  @in_progress = nil
                end
              end
            else
              context.response.status_code = 400
              context.response.print "Unknown job"
            end
          rescue exception
            context.response.status_code = 400
            context.response.print "Invalid request"
          end
        else
          context.response.status_code = 400
          context.response.print "Unknown endpoint"
        end
        @logger.warn { "Complete" }
      end

      spawn do
        Fiber.current.name = "quit-listener"
        @done.receive
        @logger.warn { "And I'm done" }
        server.close
      end

      spawn do
        Fiber.current.name = "async-monitor"
        while true
          do_sleep = true
          unless @in_progress
            if exec = @async_queue.shift?
              @in_progress = exec.name
              job = @jobs[exec.name]
              exec_job(nil, job, exec)
              @in_progress = nil
              do_sleep = false
            end
          end
          sleep 1.second if do_sleep
        end
      end

      server_port = (ENV["SERVER_PORT"]? || "3000").to_i32
      @logger.warn { "Listening on #{server_port}" }
      server.bind_tcp("0.0.0.0", server_port)
      server.listen
    end
  end
end
