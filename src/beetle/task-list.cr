require "json"

module Beetle
  private class TaskList
    include JSON::Serializable
    property tasks : Array(String) = [] of String

    def initialize(tasks : Array(String))
      tasks.each do |t|
        @tasks << t
      end
    end
  end
end
