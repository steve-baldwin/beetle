require "./job-exec"

module Beetle
  #
  # An abstract class that can be sub-classed to capture the results of a Job execution.
  # To use an archiver you instantiate it and then pass this object to the `Server#archiver=(archiver)` method.
  #
  # For example:
  # ```
  # archiver = Beetle::CSVArchiver.new(".")
  # s = Beetle::Server.new
  # s.archiver = archiver
  # :
  # s.listen
  # ```
  #
  abstract class Archiver
    #
    # Called on completion of a Job Execution.
    #
    abstract def archive(job_exec : JobExec)
  end
end
