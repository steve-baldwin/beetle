require "json"

module Beetle
  #
  # Represents the state of the server.
  #
  class ServerStatus
    include JSON::Serializable
    @[JSON::Field(converter: Time::Format.new("%F %T.%L%:z"))]
    # :nodoc:
    property timestamp : Time = Time.utc
    # :nodoc:
    property in_progress : JobExec?
    # :nodoc:
    property async_queue : Array(Exec)?
    # :nodoc:
    property tasks : Array(String) = [] of String
    # :nodoc:
    property jobs : Hash(String, Job)

    def initialize(in_progress, @async_queue, tasks : Hash(String, Task.class), @jobs)
      if in_progress
        in_progress_c = in_progress.clone
        in_progress_c.derive_stats
        @in_progress = in_progress_c
      end
      @tasks = tasks.keys
    end
  end
end
