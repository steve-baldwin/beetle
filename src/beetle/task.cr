require "json"
require "http/client"
require "log"

module Beetle
  #
  # An abstract class defining a task that is eligible to be referenced in a `Job`. When
  # creating a subclass of `Task`, you only need to define your implementation of the `#submit`
  # method.
  #
  # If you want to record any contextual information for a specific submission of a `Task`
  # you can set the `request_context` property. This property gets cleared before each
  # submission but will form part of the response and is available to an archiver.
  #
  # Here's a simple example of a `Task` subclass:
  # ```
  # class ChargeTask < Beetle::Task
  #   def sc_mc
  #     ret = "mc"
  #     if p = @params
  #       if v = p["sc_mc"]?
  #         v = v.downcase
  #         v = "sc" unless v == "mc"
  #         ret = v
  #       end
  #     end
  #     ret
  #   end
  #
  #   def submit(client : HTTP::Client)
  #     payload = Charge.new(sc_mc)
  #     @request_context = "buyer_id: #{payload.buyer_id}"
  #     log_warn("[#{@swarm_id}] #{@request_context}")
  #     client.post("/charges", headers: BCaaS.headers(sc_mc), body: payload.to_json.to_s)
  #   end
  # end
  # ```
  #
  abstract class Task
    @logger : Log
    @swarm_id : Int32
    @params : Hash(String, String)?

    # :nodoc:
    property request_context : String?

    # :nodoc:
    def initialize(@logger, @swarm_id, @params)
    end

    #
    # The `#default_host` method allows a subclass to override this method and return
    # the default host URL for a Task. It will be used unless overridden in the `Job` spec.
    #
    def default_host : String?
      nil
    end

    #
    # The `#submit` method is where you submit the http request you want to measure.
    # The `Beetle` infrastructure handles everything else.
    #
    # In the submit method, your subclass has access to the following properties:
    #  * `params` - A `Hash` of key-value pairs passed in with the `/exec` request.
    #  * `request_context` - A string you can populate with contextual data for
    # an individual request. It will be returned in the response to `/exec` and
    # made available to an archiver in the `Archiver#archive` method.
    #
    abstract def submit(client : HTTP::Client) : HTTP::Client::Response
  end
end
