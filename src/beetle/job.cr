require "json"
require "./job-task"

module Beetle
  #
  # Defines a Job eligible to be executed by `Beetle`. Instances of this class are not
  # created directly but rather by the `Server` object from the JSON payload associated
  # with the `/job` request. The fields in the JSON payload are:
  #
  #  * `name` - Defines a unique name for a job.
  #  * `host` - Defines the host to receive any http requests coming from individual tasks.
  #  * `run_for_sec` - Defines the duration (in seconds) to run a job execution. Either this or `#run_n` must be set.
  #  * `run_n` - Defines the number of requests each swarm member will make for a job execution. Either this or `#run_for_sec` must be set.
  #  * `n_swarm` - The number of processes to concurrently submit Tasks.
  #  * `tasks` - An array of one or more `JobTask`s to be executed.
  #
  # For example:
  # ```json
  # {
  #     "name": "charges_local",
  #     "host": "http://localhost:3001",
  #     "run_for_sec": 10,
  #     "tasks": [
  #         {
  #             "task_class": "BCaaS::ChargeTask",
  #             "weight": 1,
  #             "sleep_min_msec": 500,
  #             "sleep_max_msec": 1000
  #         }
  #     ]
  # }
  # ```
  #
  class Job
    include JSON::Serializable
    # :nodoc:
    property name : String
    # :nodoc:
    property host : String?
    # :nodoc:
    property run_for_sec = 0
    # :nodoc:
    property run_n = 0
    # :nodoc:
    property tasks : Array(JobTask)
    # :nodoc:
    property n_swarm = 1
  end
end
