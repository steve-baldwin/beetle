require "json"
require "./job"
require "./exec"
require "./job-task-exec"

module Beetle
  #
  # Defines a collection of statistics summarising a `Job` execution.
  # You never directly instantiate this class. A JSON
  # representation of this class is returned in the response to an `/exec`
  # request.
  #
  # If you define an `Archiver` object for a `Server`, an instance of this class
  # is also passed to the `Archiver#archive` method.
  #
  # It contains the following fields:
  #  * `id` - A unique (UUID) identifier generated each time this job is executed
  #  * `name` - See `Job`.`name`
  #  * `host` - See `Job`.`host`
  #  * `run_for_sec` - See `Job`.`run_for_sec`
  #  * `run_n` - See `Job`.`run_n`
  #  * `n_swarm` - See `Job`.`n_swarm`
  #  * `t_start` - Time (UTC) the job execution was received
  #  * `elapsed_usec` - The total elapsed time (in microseconds) to execute the job.
  # This will include server overhead to manage the job, wait for workers, etc.
  #  * `params` - See `Exec`.`params`
  #  * `error_400_on_threshold_exceeded` - See `Exec`.`error_400_on_threshold_exceeded`
  #  * `threshold_exceeded` - Boolean showing if one or more of the non-zero
  # thresholds defined for this job were exceeded
  #  * `tasks` - An array of `JobTaskExec` objects
  # For example:
  # ```json
  # {
  #   "id": "77f2d06c-be37-4bdc-98ad-02fed5acd892",
  #   "name": "charges_local_2",
  #   "host": "http://localhost:3001",
  #   "t_start": "2019-02-19 21:36:47.430+00:00",
  #   "elapsed_usec": 750782,
  #   "params": {
  #       "sc_mc": "mc"
  #   },
  #   "error_400_on_threshold_exceeded": false,
  #   "tasks": {
  #       "BCaaS::ChargeTask": {
  #           "host": "http://localhost:3001",
  #           "weight": 1,
  #           "sleep_min_msec": 500,
  #           "sleep_max_msec": 1000,
  #           "threshold_avg_usec": 0,
  #           "threshold_max_usec": 0,
  #           "n_task": 2,
  #           "n_success": 2,
  #           "n_fail": 0,
  #           "min_usec": 448,
  #           "max_usec": 1980,
  #           "avg_usec": 1214,
  #           "tot_usec": 2428,
  #           "executions": [
  #               {
  #                   "id": "80b65420-2902-4e44-8676-6e2f620003af",
  #                   "swarm_id": 1,
  #                   "t_start": "2019-02-19 21:36:47.430+00:00",
  #                   "elapsed_usec": 1980,
  #                   "request_context": "buyer_id: 5066112c-b321-40a0-ac61-a7868b1921ee",
  #                   "is_success": true
  #               },
  #               {
  #                   "id": "5f173b79-9e11-423b-9fe9-e204c669af3a",
  #                   "swarm_id": 1,
  #                   "t_start": "2019-02-19 21:36:48.180+00:00",
  #                   "elapsed_usec": 448,
  #                   "request_context": "buyer_id: 45672a98-281a-4ee3-857a-89ea27fa2dcb",
  #                   "is_success": true
  #               }
  #           ],
  #           "threshold_avg_exceeded": false,
  #           "threshold_max_exceeded": false
  #       }
  #   },
  #   "run_for_sec": 0,
  #   "run_n": 2,
  #   "n_swarm": 1,
  #   "threshold_exceeded": false
  # }
  # ```
  #
  class JobExec
    include JSON::Serializable
    # :nodoc:
    property id : String = UUID.random.to_s
    # :nodoc:
    property name : String
    # :nodoc:
    property host : String?
    # :nodoc:
    property run_for_sec = 0
    # :nodoc:
    property run_n = 0
    # :nodoc:
    property n_swarm = 1
    @[JSON::Field(converter: Time::Format.new("%F %T.%L%:z"))]
    # :nodoc:
    property t_start : Time = Time.utc
    # :nodoc:
    property elapsed_usec : Int64 = 0
    # :nodoc:
    property params : Hash(String, String) = {} of String => String
    # :nodoc:
    property max_failure_count : Int32
    # :nodoc:
    property report_failures : Bool
    # :nodoc:
    property report_progress : Bool
    # :nodoc:
    property error_400_on_threshold_exceeded : Bool
    # :nodoc:
    property n_failure : Int32 = 0
    # :nodoc:
    property threshold_exceeded = false
    # :nodoc:
    property tasks : Hash(String, Beetle::JobTaskExec) = {} of String => Beetle::JobTaskExec

    def_clone

    # :nodoc:
    def initialize(job : Job, exec : Exec)
      @name = job.name
      @host = job.host
      @run_for_sec = job.run_for_sec
      @run_n = job.run_n
      @n_swarm = job.n_swarm
      if p = exec.params
        @params = p
      end
      @max_failure_count = exec.max_failure_count > 0 ? exec.max_failure_count : Int32::MAX
      @report_failures = exec.report_failures
      @report_progress = exec.report_progress
      @error_400_on_threshold_exceeded = exec.error_400_on_threshold_exceeded
      job.tasks.each do |task|
        @tasks[task.task_class] = JobTaskExec.new(task)
      end
    end

    # :nodoc:
    def derive_stats
      @tasks.each do |task_class, exec|
        @threshold_exceeded = true if exec.derive_stats
      end
    end

    # :nodoc:
    def progress_summary : String
      summary = String.build do |s|
        first = true
        @tasks.each do |task_class, exec|
          m = IO::Memory.new
          m.printf("%s%s: %7d Σ %7d ✅ %7d ❌", first ? "" : ", ", task_class, exec.executions.size, exec.count_success, exec.count_failure)
          s << m.to_s
          first = false
        end
      end
      summary
    end

    # :nodoc:
    def clear_executions
      @tasks.each do |task_class, exec|
        exec.clear_executions
      end
    end
  end
end
