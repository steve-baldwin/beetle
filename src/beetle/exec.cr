require "json"

module Beetle
  #
  # Defines the execution of a job that had previously been registered with the
  # Beetle server (via the `/job` endpoint).

  # Instances of this class are not created directly but rather by the `Server` object from the JSON payload associated
  # with the `/exec` request.
  #
  # The fields in the JSON payload are:
  #  * `name` - Corresponds to the `name` field of the `Job` you wish to execute.
  #  * `params` - An optional collection of key-value pairs to define run-time parameters to be made accesible
  # to the `Task` objects that are executed as part of this job.
  #  * `max_failure_count` - Abort a Job Execution if I've seen this many errors (0 = no limit).
  #  * `report_progress` - Should the beetle server report on progress of this job every 10 seconds? (default `true`)
  #  * `report_failures` - Should the beetle server report on task failures? (default `true`)
  #  * `error_400_on_threshold_exceeded` - The server will respond with a response
  # code of 400 if any threshold is exceeded. Defaults to `false`.
  #  * `webhook_url` - The URL to receive the output of the Job Execution. If this is set the
  # job execution will be enqueued and run anynchronously. The `/exec` request will respond
  # immediately with `Execution request enqueued`. After the async job has been executed, the response will
  # be delivered to the webhook URL.
  #
  # The server listening on `webhook_url` should accept the JSON encoded `JobExec` payload as a `POST` request.
  #
  # If `webhook_url` is populated the `error_400_on_threshold_exceeded` value is ignored.
  #
  # For example (payload for a `/exec` request):
  # ```json
  # {
  #     "name": "charges_local",
  #     "params": {
  #         "sc_mc": "mc"
  #     }
  # }
  # ```
  # Here's an example of accessing the run-time parameters:
  # ```
  # abstract class BCaaSTask < Beetle::Task
  #   def sc_mc
  #     ret = "mc"
  #     if p = @params
  #       if v = p["sc_mc"]?
  #         v = v.downcase
  #         v = "sc" unless v == "mc"
  #         ret = v
  #       end
  #     end
  #     ret
  #   end
  # end
  # ```
  #
  class Exec
    include JSON::Serializable
    # :nodoc:
    property name : String
    # :nodoc:
    property params : Hash(String, String)?
    # :nodoc:
    property report_progress : Bool = true
    # :nodoc:
    property report_failures : Bool = true
    # :nodoc:
    property max_failure_count : Int32 = 10
    # :nodoc:
    property error_400_on_threshold_exceeded = false
    # :nodoc:
    property webhook_url : String?
  end
end
