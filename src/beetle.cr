require "./ext/http-client"
require "./beetle/**"

#
# The Beetle namespace. Classes of most interest are:
#  * `Server`
#  * `Task`
#  * `Job`
#  * `Exec`
#  * `Archiver` (and implementations for csv and database)
#
module Beetle
  # :nodoc:
  VERSION = "0.3.8"
end
