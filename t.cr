require "./src/beetle"
require "db"
require "sqlite3"

module My
  class Task1 < Beetle::Task
    def default_host
      "https://httpbin.org"
    end

    def submit(client) : HTTP::Client::Response
      client.get("/status/200%2C201%2C404")
    end
  end

  class Task2 < Beetle::Task
    def default_host
      "http://localhost:3001"
    end

    def submit(client) : HTTP::Client::Response
      client.post("/endpoint")
    end
  end
end

db = DB.open("sqlite3:./t-result.db")
archiver = Beetle::DBArchiver.new(db, "sql/sqlite3-archiver-tables.sql")
s = Beetle::Server.new
s.register_task My::Task1
s.register_task My::Task2
s.archiver = archiver
s.listen
