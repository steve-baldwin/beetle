#!/bin/sh
host=${1:-http://localhost:3000}
ls job*.json |
while read f
do
    /bin/echo -n "Loading ${f} ... "
    curl -X POST -H "Content-Type: application/json" -d @${f} ${host}/job
    /bin/echo ""
done
curl -s ${host}/jobs | python -m json.tool
