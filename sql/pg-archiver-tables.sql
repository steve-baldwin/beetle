CREATE TABLE IF NOT EXISTS beetle_job (
        id                      UUID PRIMARY KEY,
        name                    TEXT NOT NULL,
        host                    TEXT,
        run_for_sec             INT,
        run_n                   INT,
        n_swarm                 INT,
        start_ts                TIMESTAMPTZ,
        elapsed_usec            BIGINT,
        threshold_exceeded      BOOLEAN
);

CREATE TABLE IF NOT EXISTS beetle_job_param (
        job_id                  UUID,
        key                     TEXT,
        value                   TEXT,
        CONSTRAINT beetle_job_param_pkey PRIMARY KEY (job_id, key)
);

CREATE TABLE IF NOT EXISTS beetle_job_task (
        job_id                  UUID NOT NULL,
        task_class              TEXT NOT NULL,
        host                    TEXT NOT NULL,
        weight                  INT,
        sleep_min_msec          INT,
        sleep_max_msec          INT,
        threshold_avg_usec      INT,
        threshold_max_usec      INT,
        n_task                  INT NOT NULL,
        n_success               INT NOT NULL,
        n_fail                  INT NOT NULL,
        min_elapsed_usec        INT,
        max_elapsed_usec        INT,
        avg_elapsed_usec        INT,
        tot_elapsed_usec        BIGINT,
        threshold_avg_exceeded  BOOLEAN,
        threshold_max_exceeded  BOOLEAN,
        CONSTRAINT beetle_job_task_pkey PRIMARY KEY (job_id, task_class)
);

CREATE TABLE IF NOT EXISTS beetle_task_exec (
        id                      UUID PRIMARY KEY,
        job_id                  UUID NOT NULL,
        swarm_id                INT NOT NULL,
        task_class              TEXT NOT NULL,
        start_ts                TIMESTAMPTZ NOT NULL,
        elapsed_usec            INT NOT NULL,
        request_context         TEXT,
        error_info              TEXT,
        success                 BOOLEAN NOT NULL
);

