CREATE TABLE IF NOT EXISTS beetle_job (
        id                      TEXT PRIMARY KEY,
        name                    TEXT NOT NULL,
        host                    TEXT,
        run_for_sec             INTEGER,
        run_n                   INTEGER,
        n_swarm                 INT,
        start_ts                TEXT,
        elapsed_usec            INT,
        threshold_exceeded      TEXT
);

CREATE TABLE IF NOT EXISTS beetle_job_param (
        job_id                  TEXT,
        key                     TEXT,
        value                   TEXT,
        CONSTRAINT beetle_job_param_pkey PRIMARY KEY (job_id, key)
);

CREATE TABLE IF NOT EXISTS beetle_job_task (
        job_id                  TEXT NOT NULL,
        task_class              TEXT NOT NULL,
        host                    TEXT NOT NULL,
        weight                  INTEGER,
        sleep_min_msec          INTEGER,
        sleep_max_msec          INTEGER,
        threshold_avg_usec      INTEGER,
        threshold_max_usec      INTEGER,
        n_task                  INTEGER NOT NULL,
        n_success               INTEGER NOT NULL,
        n_fail                  INTEGER NOT NULL,
        min_elapsed_usec        INTEGER,
        max_elapsed_usec        INTEGER,
        avg_elapsed_usec        INTEGER,
        tot_elapsed_usec        INTEGER,
        threshold_avg_exceeded  TEXT,
        threshold_max_exceeded  BOOLEAN,
        CONSTRAINT beetle_job_task_pkey PRIMARY KEY (job_id, task_class)
);

CREATE TABLE IF NOT EXISTS beetle_task_exec (
        id                      TEXT PRIMARY KEY,
        job_id                  TEXT NOT NULL,
        swarm_id                INTEGER NOT NULL,
        task_class              TEXT NOT NULL,
        start_ts                TEXT NOT NULL,
        elapsed_usec            INTEGER NOT NULL,
        request_context         TEXT,
        error_info              TEXT,
        success                 TEXT NOT NULL
);
