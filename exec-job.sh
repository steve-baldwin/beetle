#
# Submit $1 to $2 (or http://localhost:3000)
#
f=$1
host=${2:-http://localhost:3000}
curl -s -X POST -H "Content-Type: application/json" -d @${f} ${host}/exec | python -m json.tool