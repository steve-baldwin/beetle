#
# This is a sample Webhook listener you might use with async job executions.
# It only dumps the payload to STDOUT but should give you an idea of how to
# extend it to your requirements.
# It listens on either port 3001 or whatever environment variable WEBHOOK_PORT
# says.
#
require "beetle"
require "http/server"
require "log"

def handle_job_exec(job_exec : Beetle::JobExec)
  puts job_exec.to_pretty_json
end

backend = Log::IOBackend.new(STDOUT)
t_prev = t_start = Time.local
progname = File.basename(PROGRAM_NAME)
backend.formatter = Log::Formatter.new do |entry, io|
  total_time = entry.timestamp - t_start
  delta_time = entry.timestamp - t_prev
  t_prev = entry.timestamp
  io << entry.severity.label[0]
  io << "|" << progname
  io << "|" << entry.timestamp.to_s("%H:%M:%S.%L")
  io.printf("|%09.6f|%09.6f", total_time.total_seconds, delta_time.total_seconds)
  io << "|" << entry.message
end
builder = Log::Builder.new
level = Log::Severity.parse(ENV.fetch("CRYSTAL_LOG_LEVEL", "INFO"))
builder.bind("*", level, backend)
logger = builder.for("")

server = HTTP::Server.new do |context|
  endpoint = context.request.resource
  logger.warn { "Received request on #{endpoint}" }
  case endpoint
  when "/webhook"
    if ctype = context.request.headers["Content-Type"]?
      if ctype.downcase.starts_with?("application/json")
        begin
          body = context.request.body.not_nil!.gets_to_end
          job_exec = Beetle::JobExec.from_json(body)
          handle_job_exec(job_exec)
        rescue NilAssertionError
          puts "? Empty request body"
        rescue ex : JSON::ParseException
          puts "? JSON parse failed with [#{ex.message}] on:\n#{body}"
        rescue ex
          puts ex.inspect_with_backtrace
        end
      else
        puts "? Unexpected Content-Type - [#{ctype}]"
      end
    else
      puts "? No [Content-Type] request header"
    end
  end
end

port = (ENV["WEBHOOK_PORT"]? || "3001").to_i32
logger.warn { "Listening on #{port}" }
server.bind_tcp("0.0.0.0", port)
server.listen
